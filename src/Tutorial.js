
var TutorialLayer = cc.LayerColor.extend({
    init: function( ) {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
    	this.tutorial = cc.Sprite.create( 'res/images/tutorial.png' );
	    this.tutorial.setPosition( new cc.Point( 580, 655 ) );
	    this.addChild( this.tutorial );
        cc.audioEngine.playMusic( 'res/effects/tutorial.mp3', true );
        this.addKeyboardHandlers();
        
        
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        switch( keyCode ){
        case 32:
            cc.audioEngine.playMusic( 'res/effects/tutorial.mp3', true );
            cc.director.runScene(new FirstScene());
            break;
        }
    },
    onKeyUp: function( keyCode, event ) {
    },
});
 
var TutorialScene = cc.Scene.extend({
    onEnter: function() {
        console.log();
        this._super();
        var layer = new TutorialLayer();
        layer.init();
        this.addChild( layer );
    }
});