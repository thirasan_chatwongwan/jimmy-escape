var Player = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/player.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.MOVE_STEP = 5;
        
        this.nextDirection = Player.DIR.STILL;
        this.direction = Player.DIR.STILL;
        this.command = 0;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
            if ( ! this.isPossibleToMove( this.nextDirection ) ) {
                    this.nextDirection = Player.DIR.STILL;
            }
            this.setNextDirection();
            this.direction = this.nextDirection;
            switch ( this.direction ) {
            case Player.DIR.UP:
                this.y += this.MOVE_STEP;
                break;
            case Player.DIR.DOWN:
                this.y -= this.MOVE_STEP;
                break;
            case Player.DIR.LEFT:
                this.x -= this.MOVE_STEP;
                break;
            case Player.DIR.RIGHT:
                this.x += this.MOVE_STEP;
                break;
            }
            GameLayer.playerx = this.x;
            GameLayer.playery = this.y;
            this.updatePosition();
    },
    setNextDirection: function() {
        if( this.isPossibleToMove( this.command ) && (this.x-60)%40 == 0 && (this.y-60)%40 == 0){
                this.nextDirection = this.command;
        }
    },
    isAtCenter: function() {
        return true;
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
    isPossibleToMove: function( dir ) {
        if ( dir == Player.DIR.STILL ) {
            return true;
        }
        if( dir == Player.DIR.UP){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-60)/40+1) );
        }
        else if( dir == Player.DIR.RIGHT){
            return ! this.maze.isWall( parseInt((this.x-60)/40)+2, parseInt((this.y-60)/40) );
        }
        else if( dir == Player.DIR.LEFT){
            return ! this.maze.isWall( parseInt((this.x-65)/40+1), parseInt((this.y-60)/40) );
        }
        else if( dir == Player.DIR.DOWN){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-65)/40) );
        }
    },
});
Player.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};