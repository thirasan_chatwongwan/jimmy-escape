
var FirstLayer = cc.LayerColor.extend({
    ctor: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
    	this.bottomPillar = cc.Sprite.create( 'res/images/firstscene.png' );
	    this.bottomPillar.setPosition( new cc.Point( 580, 655 ) );
	    this.addChild( this.bottomPillar );
        
        this.level1 = new cc.MenuItemImage('res/images/level1.png', 'res/images/level1.png',
        function(){
            cc.audioEngine.playMusic( 'res/effects/mainsoundtrack.mp3', true );
            level = 1;
            cc.director.runScene(new StartScene());
        },this);
        
        this.level2 = new cc.MenuItemImage('res/images/level2.png', 'res/images/level2.png',
        function(){
            cc.audioEngine.playMusic( 'res/effects/mainsoundtrack.mp3', true );
            level = 2;
            cc.director.runScene(new StartScene());
        },this);
                
        this.level3 = new cc.MenuItemImage('res/images/level3.png', 'res/images/level3.png',
        function(){
            cc.audioEngine.playMusic( 'res/effects/mainsoundtrack.mp3', true );
            level = 3;
            cc.director.runScene(new StartScene());
        },this);
        
        this.tutorial = new cc.MenuItemImage('res/images/tutorialblock.png', 'res/images/tutorialblock.png',
        function(){
            cc.audioEngine.playMusic( 'res/effects/mainsoundtrack.mp3', true );
            cc.director.runScene(new TutorialScene());
        },this);
        
        this.level1.setPosition( new cc.Point(580, 850));
        this.level2.setPosition( new cc.Point(580, 760));
        this.tutorial.setPosition( new cc.Point(580, 455));
        this.level3.setPosition( new cc.Point(580, 650));
        this.menu = new cc.Menu(this.level1,this.level2,this.tutorial,this.level3);
        this.menu.setPosition( new cc.Point( 0, 0 ) )
        this.addChild(this.menu);
        
        cc.audioEngine.playMusic( 'res/effects/mainsoundtrack.mp3', true );
        
    },
});
 
var FirstScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new FirstLayer();
        layer.init();
        this.addChild( layer );
    }
});
var level = 0;