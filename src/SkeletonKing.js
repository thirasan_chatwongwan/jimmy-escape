var SkeletonKing = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/skeletonking.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.nextDirection = SkeletonKing.DIR.STILL;
        this.direction = SkeletonKing.DIR.STILL;
        
        this.cooldown = 700;
        this.spawnskeleton = false;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
            if(! this.isPossibleToMove( this.nextDirection)){
                this.command = this.nextDirection;
                while(!this.isPossibleToMove(this.command)){
                    this.command = parseInt(1 + Math.random()*4);
                    if(this.command == 1 && this.nextDirection == 2)
                        this.command = 2 ;
                    else if(this.command == 2 && this.nextDirection == 1)
                        this.command = 1 ;
                    else if(this.command == 3 && this.nextDirection == 4)
                        this.command = 4 ;
                    else if(this.command == 4 && this.nextDirection == 3)
                        this.command = 3 ;
                }
                this.nextDirection = this.command
            }
            else if ( ((this.x-60)%40 == 0 && (this.y-60)%40 == 0) || this.direction == 0){
                this.findNextMove();
            }
                                    
            this.direction = this.nextDirection;
            this.move();
            this.updatePosition();

            if(this.cooldown == 0){
                this.spawnskeleton = true;
                this.cooldown = 700;
            }
            this.cooldown--;
    },
    isAtCenter: function() {
        return true;
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
    isPossibleToMove: function( dir ) {
        if ( dir == SkeletonKing.DIR.STILL ) {
            return true;
        }
        if( dir == SkeletonKing.DIR.UP){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-60)/40+1) );
        }
        else if( dir == SkeletonKing.DIR.RIGHT){
            return ! this.maze.isWall( parseInt((this.x-60)/40)+2, parseInt((this.y-60)/40) );
        }
        else if( dir == SkeletonKing.DIR.LEFT){
            return ! this.maze.isWall( parseInt((this.x-65)/40+1), parseInt((this.y-60)/40) );
        }
        else if( dir == SkeletonKing.DIR.DOWN){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-65)/40) );
        }
    },
    findNextMove: function(){
        for(i = 1; i <= 4; i++) {
            if ( this.isPossibleToMove( i ) ){
                if(Math.random()*100 > 50){
                    switch ( i ) {
                        case SkeletonKing.DIR.UP:
                            if( this.nextDirection == SkeletonKing.DIR.DOWN){
                                i = SkeletonKing.DIR.DOWN;
                            }
                            break;
                        case SkeletonKing.DIR.DOWN:
                            if( this.nextDirection == SkeletonKing.DIR.UP){
                                i = SkeletonKing.DIR.UP;
                            }
                            break;
                        case SkeletonKing.DIR.LEFT:
                            if( this.nextDirection == SkeletonKing.DIR.RIGHT){
                                i = SkeletonKing.DIR.RIGHT;
                            }
                            break;
                        case SkeletonKing.DIR.RIGHT:
                            if( this.nextDirection == SkeletonKing.DIR.LEFT){
                                i = SkeletonKing.DIR.LEFT;
                            }
                            break;
                    }
                    this.nextDirection = i;
                    break;
                }
            }
        }
    },
    move: function(){
        switch ( this.direction ) {
            case SkeletonKing.DIR.UP:
                this.y += SkeletonKing.MOVE_STEP;
                break;
            case SkeletonKing.DIR.DOWN:
                this.y -= SkeletonKing.MOVE_STEP;
                break;
            case SkeletonKing.DIR.LEFT:
                this.x -= SkeletonKing.MOVE_STEP;
                break;
            case SkeletonKing.DIR.RIGHT:
                this.x += SkeletonKing.MOVE_STEP;
                break;
        }
    }
});
SkeletonKing.MOVE_STEP = 2.5;
SkeletonKing.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};

var command = 0;