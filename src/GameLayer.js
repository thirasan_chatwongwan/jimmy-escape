
var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        
    	this.maze = new Maze();
	    this.maze.setPosition( cc.p( 0, 40 ) );
        this.addChild( this.maze );
        
        this.createBoost();
        score = 0;
        
        this.createPlayer();
        this.createObject();
        
        cc.audioEngine.playMusic( 'res/effects/footsteps.mp3', true );
    },
    createObject: function(){
        this.skeletonarmy = new Array();
        this.guardianarmy = new Array();
        this.firegang = new Array();
        
        this.scheduleUpdate();
        
        this.timeToSpawnDestroyer = 0;
        this.numDestroyer = 1;
        this.boostTime = 0;
        this.numberofskeleton = 0;
        this.numguardian = 0;
        this.numfire = 0;
        
        this.jesus = new Jesus(14*40 + 60, 13*40 + 60);
        this.maze.addChild( this.jesus );
        this.jesus.scheduleUpdateWithPriority(3);
        this.jesus.setMaze( this.maze );
        this.jesus.changePosition();
        
        this.skeletonking = new SkeletonKing(13*40 + 60, 27*40 + 60);
        this.maze.addChild( this.skeletonking );
        this.skeletonking.scheduleUpdateWithPriority(2);
        this.skeletonking.setMaze( this.maze );
        
        this.createItemSlow();
        this.createLabel();
    },
    createPlayer: function(){
        this.player = new Player( 14*40 + 60, 13*40 + 60 );
        this.maze.addChild( this.player );
        
        this.player.scheduleUpdateWithPriority(1);
        this.addKeyboardHandlers();
        this.player.setMaze( this.maze );
    },
    createLabel: function(){
        this.scoreLabel = cc.LabelTTF.create( " Score : 0", 'Arial', 40 );
	    this.scoreLabel.setPosition( new cc.Point( 1000, 1270 ) );
	    this.addChild( this.scoreLabel );
        
        this.numJesusLabel = cc.LabelTTF.create( "Num Jesus: 0", 'Arial', 40 );
	    this.numJesusLabel.setPosition( new cc.Point( 550, 1270 ) );
	    this.addChild( this.numJesusLabel );
        
        this.levelLabel = cc.LabelTTF.create( "Level: "+ level, 'Arial', 40 );
	    this.levelLabel.setPosition( new cc.Point( 100, 1270 ) );
	    this.addChild( this.levelLabel );
        
        this.pauseLabel = cc.LabelTTF.create( "", 'Arial', 80 );
	    this.pauseLabel.setPosition( new cc.Point( 580, 655 ) );
	    this.addChild( this.pauseLabel );
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    createSkeleton: function() {
        this.skeletonarmy[this.numberofskeleton] = new Skeleton( this.skeletonking.x, this.skeletonking.y );
        this.maze.addChild( this.skeletonarmy[this.numberofskeleton] );
        this.skeletonarmy[this.numberofskeleton].scheduleUpdate();
        this.skeletonarmy[this.numberofskeleton].setMaze( this.maze );
        this.numberofskeleton++;
        
    },
    createGuardian: function() {
        this.guardianarmy[this.numguardian] = new Guardian( this.skeletonking.x, this.skeletonking.y );
        this.maze.addChild( this.guardianarmy[this.numguardian] );
        this.guardianarmy[this.numguardian].scheduleUpdate();
        this.guardianarmy[this.numguardian].setMaze( this.maze );
        this.numguardian++;
        
    },
    createFireball: function() {
        for( i = 0 ; i <=3 ; i++){
            this.firegang[i+this.numfire] = new Fireball( destroyerx, destroyery );
            this.firegang[i+this.numfire].direction = i;
            this.addChild( this.firegang[i+this.numfire] );
            this.firegang[i+this.numfire].scheduleUpdate();
        }
        this.numfire += 4;
    },
    createItemSlow: function() {
        this.itemSlow = new Array();
        itemslowx = 0;
        itemslowy = 0;
        while(this.maze.isWall( itemslowx+1, itemslowy)){
            itemslowx = parseInt(Math.random()*26);
            itemslowy = parseInt(Math.random()*27);
        }
        this.itemSlow[0] = new ItemSlow( itemslowx*40 + 60, itemslowy*40 + 60 );
        this.maze.addChild( this.itemSlow[0] );

        this.itemSlow[0].scheduleUpdate();
        this.itemSlow[0].setMaze( this.maze );
        
        itemslowx = 0;
        itemslowy = 0;
        while(this.maze.isWall( itemslowx+1, itemslowy)){
            itemslowx = parseInt(Math.random()*26);
            itemslowy = parseInt(Math.random()*27);
        }
        this.itemSlow[1] = new ItemSlow( itemslowx*40 + 60, itemslowy*40 + 60 );
        this.maze.addChild( this.itemSlow[1] );

        this.itemSlow[1].scheduleUpdate();
        this.itemSlow[1].setMaze( this.maze );
    },
    createBoost: function() {
        this.boost = new Array();
        this.boost[0] = new Boost(4*40 + 60, 5*40 + 60);
        this.maze.addChild( this.boost[0] );

        this.boost[0].scheduleUpdateWithPriority(99);
        this.boost[0].setMaze( this.maze );
        
        this.boost[1] = new Boost(24*40 + 60, 19*40 + 60);
        this.maze.addChild( this.boost[1] );

        this.boost[1].scheduleUpdateWithPriority(100);
        this.boost[1].setMaze( this.maze );
    },
    onKeyDown: function( keyCode, event ) {
        switch( keyCode ) {
        case cc.KEY.left:
            this.player.command = Player.DIR.LEFT;
            break;
        case cc.KEY.right:
            this.player.command = Player.DIR.RIGHT;
            break;
        case cc.KEY.up:
            this.player.command = Player.DIR.UP;
            break;
        case cc.KEY.down:
            this.player.command = Player.DIR.DOWN;
            break;
        case 32:
            this.pause();
            break;
        case 13:
            this.continuous();
            break;
        }
    },
    pause: function(){
        this.unscheduleUpdate();
        this.player.unscheduleUpdate();
        if(this.numDestroyer == 0)
            this.destroyer.unscheduleUpdate();
        this.skeletonking.unscheduleUpdate();
        for(i = 0;i<this.skeletonarmy.length;i++){
            this.skeletonarmy[i].unscheduleUpdate();
        }
        for(i = 0;i<this.firegang.length;i++){
            this.firegang[i].unscheduleUpdate();
        }
        for(i = 0;i<this.firegang.length;i++){
            this.firegang[i].unscheduleUpdate();
        }
        this.pauseLabel.setString( " Pause : Enter to continuous " );
    },
    continuous: function(){
        this.scheduleUpdate();
        this.player.scheduleUpdate();
        if(this.numDestroyer == 0)
            this.destroyer.scheduleUpdate();
        this.skeletonking.scheduleUpdate();
        for(i = 0;i<this.skeletonarmy.length;i++){
            this.skeletonarmy[i].scheduleUpdate();
        }
        for(i = 0;i<this.firegang.length;i++){
            this.firegang[i].scheduleUpdate();
        }
        this.pauseLabel.setString( "" );
    },
    onKeyUp: function( keyCode, event ) {
    },
    update: function( dt ) {
        if(level == 3)
            this.createSkeleton();
        if(this.numDestroyer <=0){
            this.hitDestroyer();
            destroyerx = this.destroyer.x;
            destroyery = this.destroyer.y;
            if(level == 2){
                if(score % 550 < 1)
                    this.createFireball();
            }
        }
        this.checkHit();
        
        if(level == 1)
            score++;
        else if(level == 2)
            score = score +2;
        else if(level == 3)
            score = score +10;
        this.scoreLabel.setString( "Score : " + (score+1) );
        this.numJesusLabel.setString( "Num Jesus : " + (6-this.jesus.powerchange) );
    },
    checkHit: function(){
        this.checkspawn();
        this.spawnDestroyer();
        this.hitItemSlow();
        this.hitBoost();
        this.hitFireball();
        this.hitSkeleton();
        this.hitGuardian();
        this.hitSkeletonKing();
        this.clearFireball();
        
        if(this.jesus.powerchange==6){
            this.clearSkeleton();
            this.clearGuardian();
            this.jesus.powerchange = 0;
        }
        
    },
    checkspawn: function(){
        if(this.skeletonking.spawnskeleton == true && ((this.skeletonking.x-60)%40 == 0 && (this.skeletonking.y-60)%40 == 0)){
            if(level == 2){
                if(Math.random()*100 < 40)
                    this.createGuardian();
                else
                    this.createSkeleton();
            }
            else if(level == 1)
                this.createSkeleton();
            this.skeletonking.spawnskeleton = false;
        }
        
    },
    clearSkeleton: function(){
            for(i = 0;i<this.skeletonarmy.length;i++){
                this.skeletonarmy[i].unscheduleUpdate();
                this.skeletonarmy[i].x = 2000;
                this.skeletonarmy[i].y = 2000;
                this.removeChild( this.skeletonarmy[i] );
            }
            this.numberofskeleton = 0;
    },
    clearFireball: function(){
            for(i = 0;i<this.firegang.length;i++){
                if(this.firegang[i].x > 1160 || this.firegang[i].x < 0 || this.firegang[i].y>1310 || this.firegang[i].y<0){
                    this.firegang[i].unscheduleUpdate();
                    this.firegang[i].x = 2000;
                    this.firegang[i].y = 2000;
                    this.removeChild( this.firegang[i] );
                }
            }
    },
    clearGuardian: function(){
            for(j = 0;j<this.guardianarmy.length;j++){
                this.guardianarmy[j].life--;
            }
            for(i = 0;i<this.guardianarmy.length;i++){
                if(this.guardianarmy[i].life == 0){
                    this.guardianarmy[i].unscheduleUpdate();
                    this.guardianarmy[i].x = 2000;
                    this.guardianarmy[i].y = 2000;
                    this.removeChild( this.guardianarmy[i] );
                }
            }
            this.numberofskeleton = 0;
    },
    hitSkeletonKing: function(){
            if(this.player.x+20 >= this.skeletonking.x-20 && this.player.x+20 <= this.skeletonking.x+20 && this.player.y == this.skeletonking.y)
                this.endGame(i);
            if(this.player.x-20 <= this.skeletonking.x+20 && this.player.x+20 >= this.skeletonking.x-20 && this.player.y == this.skeletonking.y)
                this.endGame(i);
            if(this.player.x == this.skeletonking.x && this.player.y+20 >= this.skeletonking.y-20 && this.player.y-20 <= this.skeletonking.y+20)
                this.endGame(i);
            if(this.player.x == this.skeletonking.x && this.player.y-20 <= this.skeletonking.y+20 && this.player.y-20 >= this.skeletonking.y-20)
                this.endGame(i);
    },
    spawnDestroyer: function() {
        if(this.timeToSpawnDestroyer >= 300 && this.numDestroyer >0){
            this.numDestroyer--;
            
            this.destroyer = new Destroyer( 13*40 + 60, 13*40 + 60 );
            this.maze.addChild( this.destroyer );

            this.destroyer.scheduleUpdate();
            cc.audioEngine.playMusic( 'res/effects/footsteps.mp3', true );
            cc.audioEngine.playMusic( 'res/effects/piratesoundtrack.mp3', true );
        }
        else if(this.numDestroyer >0)
            this.timeToSpawnDestroyer++;
    },
    endGame: function(){
        this.player.unscheduleUpdate();
        this.unscheduleUpdate();
        cc.audioEngine.playMusic( 'res/effects/piratesoundtrack.mp3', true );
        cc.director.runScene(new EndScene(parseInt(this.scoreLabel.getString())));
    },
    hitDestroyer: function(){
        if(this.player.x+20 >= this.destroyer.x-35 && this.player.x+20 <= this.destroyer.x+35 && this.player.y+20 >= this.destroyer.y-35 && this.player.y-20 <= this.destroyer.y+40)
            this.endGame(i);
        if(this.player.x-35 <= this.destroyer.x+35 && this.player.x+20 >= this.destroyer.x-35 && this.player.y-20 <= this.destroyer.y+35 && this.player.y-20 >= this.destroyer.y+40)
            this.endGame(i);
    },
    hitSkeleton: function(){
        for(i = 0;i < this.skeletonarmy.length;i++){
            if(this.player.x+20 >= this.skeletonarmy[i].x-20 && this.player.x+20 <= this.skeletonarmy[i].x+20 && this.player.y == this.skeletonarmy[i].y)
                this.endGame(i);
            if(this.player.x-20 <= this.skeletonarmy[i].x+20 && this.player.x+20 >= this.skeletonarmy[i].x-20 && this.player.y == this.skeletonarmy[i].y)
                this.endGame(i);
            if(this.player.x == this.skeletonarmy[i].x && this.player.y+20 >= this.skeletonarmy[i].y-20 && this.player.y-20 <= this.skeletonarmy[i].y+20)
                this.endGame(i);
            if(this.player.x == this.skeletonarmy[i].x && this.player.y-20 <= this.skeletonarmy[i].y+20 && this.player.y-20 >= this.skeletonarmy[i].y-20)
                this.endGame(i);
        }
    },
    hitGuardian: function(){
        for(i = 0;i < this.guardianarmy.length;i++){
            if(this.player.x+15 >= this.guardianarmy[i].x-15 && this.player.x+15 <= this.guardianarmy[i].x+15 && this.player.y == this.guardianarmy[i].y)
                this.endGame(i);
            if(this.player.x-15 <= this.guardianarmy[i].x+15 && this.player.x+15 >= this.guardianarmy[i].x-15 && this.player.y == this.guardianarmy[i].y)
                this.endGame(i);
            if(this.player.x == this.guardianarmy[i].x && this.player.y+20 >= this.guardianarmy[i].y-20 && this.player.y-20 <= this.guardianarmy[i].y+15)
                this.endGame(i);
            if(this.player.x == this.guardianarmy[i].x && this.player.y-20 <= this.guardianarmy[i].y+20 && this.player.y-20 >= this.guardianarmy[i].y-15)
                this.endGame(i);
        }
    },
    hitFireball: function(){
        for(i = 0;i < this.firegang.length;i++){
            if(this.player.x >= this.firegang[i].x-20 && this.player.x <= this.firegang[i].x+20 && this.player.y >= (this.firegang[i].y-10)-30 && this.player.y <= (this.firegang[i].y-10)+ 10)
            this.endGame(i);
        }
    },
    hitItemSlow: function() {
        for(i = 0;i<this.itemSlow.length;i++){
            if(this.itemSlow[i].pickSlow == true && this.numDestroyer <=0){
                if(this.destroyer.velocity<=1.5)
                    this.destroyer.velocity = 0;
                else
                    this.destroyer.velocity -= 1.5;
                this.itemSlow[i].pickSlow = false;
            }
        }
    },
    touchJesus: function() {
        if(this.jesus.pickJesus == true){
            this.skeletonking.cooldown = 800;
            this.jesus.pickJesus = false;
        }
    },
    hitBoost: function() {
        for(i = 0;i<this.boost.length;i++){
            if(this.boost[i].pickBoost == true){
                this.player.MOVE_STEP = 8;
                this.boost[i].pickBoost = false;
                this.boostTime = 120;
            }
        }
        this.boostTime--;
        if(this.boostTime<=0 && this.player.x % 5 == 0 && this.player.y % 5 == 0)
            this.player.MOVE_STEP = 5;
    },
});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});
var playerx = 0,playery = 0,score = 0,destroyerx = 0, destroyery = 0;