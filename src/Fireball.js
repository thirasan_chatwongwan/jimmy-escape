var Fireball = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/fireball.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.direction = 0;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        if(this.direction == 0)
            this.x+= 2;
        else if(this.direction == 1)
            this.x-= 2;
        else if(this.direction == 2)
            this.y+= 2;
        else if(this.direction == 3)
            this.y-= 2;
        this.updatePosition();
    },
});