var Guardian = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/guardian.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.direction = 0;
        this.life = 2;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        if(score%75 < 1){
                if(this.isPlayerInVision()){
                    this.move();
                }
            
        }
        this.updatePosition();
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
    isPlayerInVision: function(){
        for(i = 1;i<=4;i++){
            switch ( i ) {
            case Guardian.DIR.UP:
                if(this.searchUpSide())
                    return true;
                break;
            case Guardian.DIR.DOWN:
                if(this.searchDownSide())
                    return true;
                break;
            case Guardian.DIR.LEFT:
                if(this.searchLeftSide())
                    return true;
                break;
            case Guardian.DIR.RIGHT:
                if(this.searchRightSide())
                    return true;
                break;
            }
                
        }
    },
    searchRightSide: function(){
        var linex = 0;
        for(j = (this.x-60)/40;j <= 29; j += 1){
            if(this.maze.isWall( j+1  ,parseInt((this.y-60)/40) )){
                linex = j *40 + 60;
                break;
            }
        }
            if(GameLayer.playerx-20 <= linex && (this.y-60)/40 == (GameLayer.playery-60)/40 && GameLayer.playerx > this.x  ){
                this.direction = Guardian.DIR.RIGHT;
                return true;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40+1 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.RIGHT;
                return true;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40-1 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.RIGHT;
                return true;
            }
        
    },
    searchLeftSide: function(){
        var linex = 0;
        for(j = (this.x-60)/40;j >= 0; j -= 1){
            if(this.maze.isWall( j+1  ,parseInt((this.y-60)/40) )){
                linex = j *40 + 60;
                break;
            }
        }
            if(GameLayer.playerx+20 >= linex && (this.y-60)/40 == (GameLayer.playery-60)/40 && GameLayer.playerx < this.x  ){
                this.direction = Guardian.DIR.LEFT;
                return true;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40+1 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.LEFT;
                return true;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40-1 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.LEFT;
                return true;
            }
    },
    searchDownSide: function(){
        var liney = 0;
        for(j = (this.y-60)/40;j >= 0; j -= 1){
            if(this.maze.isWall( parseInt((this.x-60)/40)+1, j )){
                liney = j*40 + 60;
                break;
            }
        }
            if(this.x == GameLayer.playerx && GameLayer.playery+20 >= liney && GameLayer.playery < this.y){
                this.direction = Guardian.DIR.DOWN;
                return true;
            }
            if((this.x-60)/40+1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40) {
                this.direction = Guardian.DIR.DOWN;
                return true;
            }
            if((this.x-60)/40-1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.DOWN;
                return true;
            }
    },
    searchUpSide: function(){
        var liney = 0;
        for(j = (this.y-60)/40;j <= 30; j += 1){
            if(this.maze.isWall( parseInt((this.x-60)/40)+1, j )){
                liney = j*40 + 60;
                break;
            }
        }
            if(this.x == GameLayer.playerx && GameLayer.playery-20 <= liney && GameLayer.playery > this.y){
                this.direction = Guardian.DIR.UP;
                return true;
            }
            if((this.x-60)/40+1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40) {
                this.direction = Guardian.DIR.UP;
                return true;
            }
            if((this.x-60)/40-1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40){
                this.direction = Guardian.DIR.UP;
                return true;
            }
    },
    move: function(){
        switch ( this.direction ) {
            case Guardian.DIR.UP:
                this.y += Guardian.MOVE_STEP;
                break;
            case Guardian.DIR.DOWN:
                this.y -= Guardian.MOVE_STEP;
                break;
            case Guardian.DIR.LEFT:
                this.x -= Guardian.MOVE_STEP;
                break;
            case Guardian.DIR.RIGHT:
                this.x += Guardian.MOVE_STEP;
                break;
        }
    }
});
Guardian.MOVE_STEP = 40;
Guardian.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};