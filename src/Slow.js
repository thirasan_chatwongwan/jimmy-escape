var ItemSlow = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/slow.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.pickSlow = false;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        if(GameLayer.playerx+20 >= this.x-15 && GameLayer.playerx+20 <= this.x+15 && GameLayer.playery == this.y){
            this.changePosition();
            this.pickSlow = true;
        }
        if(GameLayer.playerx-20 <= this.x+15 && GameLayer.playerx+20 >= this.x-15 && GameLayer.playery == this.y){
            this.changePosition();
            this.pickSlow = true;
        }
        if(GameLayer.playerx == this.x && GameLayer.playery+20 >= this.y-15 && GameLayer.playery-20 <= this.y+15){
            this.changePosition();
            this.pickSlow = true
        }
        if(GameLayer.playerx == this.x && GameLayer.playery-20 <= this.y+15 && GameLayer.playery-20 >= this.y-15){
            this.changePosition();
            this.pickSlow = true;
        }
        this.updatePosition();
    },
    changePosition: function() {
        itemslowx = 0;
        itemslowy = 0;
        while(this.maze.isWall( itemslowx+1, itemslowy)){
            itemslowx = parseInt(Math.random()*26);
            itemslowy = parseInt(Math.random()*27);
        }
        this.x = itemslowx*40 + 60;
        this.y = itemslowy*40 + 60;
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
});