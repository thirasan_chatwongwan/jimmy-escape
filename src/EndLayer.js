
var EndLayer = cc.LayerColor.extend({
    init: function( ) {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
    	this.gameover = cc.Sprite.create( 'res/images/gameover.png' );
	    this.gameover.setPosition( new cc.Point( 580, 655 ) );
	    this.addChild( this.gameover );
        cc.audioEngine.playMusic( 'res/effects/gameover.mp3', true );
        this.addKeyboardHandlers();
        
        this.scoreLabel = cc.LabelTTF.create( "your score is " + score , 'Arial', 40 );
	    this.scoreLabel.setPosition( new cc.Point( 980, 1270 ) );
	    this.addChild( this.scoreLabel );
        
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        switch( keyCode ){
        case 32:
            cc.audioEngine.playMusic( 'res/effects/gameover.mp3', true );
            cc.director.runScene(new StartScene());
            break;
        case 13:
            cc.audioEngine.playMusic( 'res/effects/gameover.mp3', true );
            cc.director.runScene(new FirstScene());
            break;
        }
    },
    onKeyUp: function( keyCode, event ) {
    },
});
 
var EndScene = cc.Scene.extend({
    onEnter: function() {
        console.log();
        this._super();
        var layer = new EndLayer();
        layer.init();
        this.addChild( layer );
    }
});