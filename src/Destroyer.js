var Destroyer = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/destroyer.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.velocity = 1;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        this.velocity += 0.002;
        if(GameLayer.playerx > this.x)
            this.x += this.velocity;
        else
            this.x -= this.velocity;
        if(GameLayer.playery > this.y)
            this.y += this.velocity;
        else
            this.y -= this.velocity;
        this.updatePosition();
    },
});