var Skeleton = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/skeleton.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.nextDirection = Skeleton.DIR.STILL;
        this.direction = Skeleton.DIR.STILL;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
            if(! this.isPossibleToMove( this.nextDirection)){
                this.command = this.nextDirection;
                while(!this.isPossibleToMove(this.command)){
                    this.command = parseInt(1 + Math.random()*4);
                    if(this.command == 1 && this.nextDirection == 2)
                        this.command = 2 ;
                    else if(this.command == 2 && this.nextDirection == 1)
                        this.command = 1 ;
                    else if(this.command == 3 && this.nextDirection == 4)
                        this.command = 4 ;
                    else if(this.command == 4 && this.nextDirection == 3)
                        this.command = 3 ;
                }
                this.nextDirection = this.command
            }
            else if ( ((this.x-60)%40 == 0 && (this.y-60)%40 == 0) || this.direction == 0){
                this.findNextMove();
            }
            if ((this.x-60)%40 == 0 && (this.y-60)%40 == 0)
                this.isPlayerInVision();
            if ( ! this.isPossibleToMove( this.nextDirection ) ) {
                this.nextDirection = Skeleton.DIR.STILL;
            }
            this.direction = this.nextDirection;
            this.move();
            this.updatePosition();
    },
    isAtCenter: function() {
        return true;
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
    isPossibleToMove: function( dir ) {
        if ( dir == Skeleton.DIR.STILL ) {
            return true;
        }
        if( dir == Skeleton.DIR.UP){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-60)/40+1) );
        }
        else if( dir == Skeleton.DIR.RIGHT){
            return ! this.maze.isWall( parseInt((this.x-60)/40)+2, parseInt((this.y-60)/40) );
        }
        else if( dir == Skeleton.DIR.LEFT){
            return ! this.maze.isWall( parseInt((this.x-65)/40+1), parseInt((this.y-60)/40) );
        }
        else if( dir == Skeleton.DIR.DOWN){
            return ! this.maze.isWall( parseInt((this.x-60)/40+1), parseInt((this.y-65)/40) );
        }
    },
    findNextMove: function(){
        for(i = 1; i <= 4; i++) {
            if ( this.isPossibleToMove( i ) ){
                if(Math.random()*100 > 50){
                    switch ( i ) {
                        case Skeleton.DIR.UP:
                            if( this.nextDirection == Skeleton.DIR.DOWN){
                                i = Skeleton.DIR.DOWN;
                            }
                            break;
                        case Skeleton.DIR.DOWN:
                            if( this.nextDirection == Skeleton.DIR.UP){
                                i = Skeleton.DIR.UP;
                            }
                            break;
                        case Skeleton.DIR.LEFT:
                            if( this.nextDirection == Skeleton.DIR.RIGHT){
                                i = Skeleton.DIR.RIGHT;
                            }
                            break;
                        case Skeleton.DIR.RIGHT:
                            if( this.nextDirection == Skeleton.DIR.LEFT){
                                i = Skeleton.DIR.LEFT;
                            }
                            break;
                    }
                    this.nextDirection = i;
                    break;
                }
            }
        }
    },
    isPlayerInVision: function(){
        for(i = 1;i<=4;i++){
            switch ( i ) {
            case Skeleton.DIR.UP:
                this.searchUpSide();
                break;
            case Skeleton.DIR.DOWN:
                this.searchDownSide();
                break;
            case Skeleton.DIR.LEFT:
                this.searchLeftSide();
                break;
            case Skeleton.DIR.RIGHT:
                this.searchRightSide();
                break;
            }
                
        }
    },
    searchRightSide: function(){
        var linex = 0;
        for(j = (this.x-60)/40;j <= 29; j += 1){
            if(this.maze.isWall( j+1  ,parseInt((this.y-60)/40) )){
                linex = j *40 + 60;
                break;
            }
        }
            if(GameLayer.playerx-20 <= linex && (this.y-60)/40 == (GameLayer.playery-60)/40 && GameLayer.playerx > this.x  ){
                this.nextDirection = Skeleton.DIR.RIGHT;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40+1 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.RIGHT;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40-1 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.RIGHT;
            }
        
    },
    searchLeftSide: function(){
        var linex = 0;
        for(j = (this.x-60)/40;j >= 0; j -= 1){
            if(this.maze.isWall( j+1  ,parseInt((this.y-60)/40) )){
                linex = j *40 + 60;
                break;
            }
        }
            if(GameLayer.playerx+20 >= linex && (this.y-60)/40 == (GameLayer.playery-60)/40 && GameLayer.playerx < this.x  ){
                this.nextDirection = Skeleton.DIR.LEFT;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40+1 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.LEFT;
            }
            if((linex-60)/40 == (GameLayer.playerx-60)/40 && (this.y-60)/40-1 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.LEFT;
            }
    },
    searchDownSide: function(){
        var liney = 0;
        for(j = (this.y-60)/40;j >= 0; j -= 1){
            if(this.maze.isWall( parseInt((this.x-60)/40)+1, j )){
                liney = j*40 + 60;
                break;
            }
        }
            if(this.x == GameLayer.playerx && GameLayer.playery+20 >= liney && GameLayer.playery < this.y){
                this.nextDirection = Skeleton.DIR.DOWN;
            }
            if((this.x-60)/40+1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40) {
                this.nextDirection = Skeleton.DIR.DOWN;
            }
            if((this.x-60)/40-1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.DOWN;
            }
    },
    searchUpSide: function(){
        var liney = 0;
        for(j = (this.y-60)/40;j <= 30; j += 1){
            if(this.maze.isWall( parseInt((this.x-60)/40)+1, j )){
                liney = j*40 + 60;
                break;
            }
        }
            if(this.x == GameLayer.playerx && GameLayer.playery-20 <= liney && GameLayer.playery > this.y){
                this.nextDirection = Skeleton.DIR.UP;
            }
            if((this.x-60)/40+1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40) {
                this.nextDirection = Skeleton.DIR.UP;
            }
            if((this.x-60)/40-1 == (GameLayer.playerx-60)/40 && (liney-60)/40 == (GameLayer.playery-60)/40){
                this.nextDirection = Skeleton.DIR.UP;
            }
    },
    move: function(){
        switch ( this.direction ) {
            case Skeleton.DIR.UP:
                this.y += Skeleton.MOVE_STEP;
                break;
            case Skeleton.DIR.DOWN:
                this.y -= Skeleton.MOVE_STEP;
                break;
            case Skeleton.DIR.LEFT:
                this.x -= Skeleton.MOVE_STEP;
                break;
            case Skeleton.DIR.RIGHT:
                this.x += Skeleton.MOVE_STEP;
                break;
        }
    }
});
Skeleton.MOVE_STEP = 5;
Skeleton.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};

var command = 0;