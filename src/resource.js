var res = {
    HelloWorld_png : "res/HelloWorld.png",
    player_png : "res/images/player.png",
    skeleton_png : "res/images/skeleton.png",
    destroyer_png: "res/images/destroyer.png",
    slow_png: "res/images/slow.png",
    boost_png: "res/images/boost.png",
    skeletonking_png: "res/images/skeletonking.png",
    jesus_png: "res/images/jesus.png",
    gameover_png: "res/images/gameover.png",
    level1_png: 'res/images/level1.png',
    level2_png: 'res/images/level2.png',
    level3_png: 'res/images/level3.png',
    firstscene_png: 'res/images/firstscene.png',
    fireball_png: 'res/images/fireball.png',
    tutorial_png: 'res/images/tutorial.png',
    tutorialblock_png: 'res/images/tutorialblock.png',
    guardian_png: 'res/images/guardian.png',
    effects_fall: 'res/effects/gameover.mp3',
    effects_piratesoundtrack: 'res/effects/piratesoundtrack.mp3',
    effects_mainsoundtrack: 'res/effects/mainsoundtrack.mp3',
    effects_footsteps: 'res/effects/footsteps.mp3',
    effects_tutorial: 'res/effects/tutorial.mp3',
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
