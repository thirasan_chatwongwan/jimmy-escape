var Boost = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/boost.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.pickBoost = false;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        if(GameLayer.playerx == this.x && GameLayer.playery == this.y){
            this.pickBoost = true;
        }
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
});