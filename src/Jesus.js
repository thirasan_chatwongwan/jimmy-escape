var Jesus = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/jesus.png' );
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.powerchange = 0;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    update: function( dt ) {
        if(GameLayer.playerx+20 >= this.x-15 && GameLayer.playerx+20 <= this.x+15 && GameLayer.playery == this.y){
            this.changePosition();
            this.powerchange++;
        }
        if(GameLayer.playerx-20 <= this.x+15 && GameLayer.playerx+20 >= this.x-15 && GameLayer.playery == this.y){
            this.changePosition();
            this.powerchange++;
        }
        if(GameLayer.playerx == this.x && GameLayer.playery+20 >= this.y-15 && GameLayer.playery-20 <= this.y+15){
            this.changePosition();
            this.powerchange++;
        }
        if(GameLayer.playerx == this.x && GameLayer.playery-20 <= this.y+15 && GameLayer.playery-20 >= this.y-15){
            this.changePosition();
            this.powerchange++;
        }
        this.updatePosition();
    },
    changePosition: function() {
        Jesusx = 0;
        Jesusy = 0;
        while(this.maze.isWall( Jesusx+1, Jesusy)){
            Jesusx = parseInt(Math.random()*26);
            Jesusy = parseInt(Math.random()*27);
        }
        this.x = Jesusx*40 + 60;
        this.y = Jesusy*40 + 60;
    },
    setMaze: function( maze ) {
        this.maze = maze;
    },
});